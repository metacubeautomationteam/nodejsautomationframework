import { BrowserTab } from "../helpers/root";
import { Login } from "../helpers/login";
import { Page, BrowserContext, Browser, ElementHandle } from "puppeteer";
import { expect } from "chai";

let browser = new BrowserTab();
let log = new Login();


describe('login', function(){
    let brw: Browser;
    let context: BrowserContext;
    let page: Page;
    
    before('create browser instance', async function(){
        this.timeout(0);
        brw = await browser.getBrowser();
        page = await browser.getPage(brw);
    });
    
    it('test login function', async function(){
        this.timeout(0);
        await log.login(page);
        const app: ElementHandle = await page.waitForSelector('nav[class*="appLauncher slds-context-bar__icon-action"]');
        expect(app).to.exist;
    });

    after('close browser', async function(){
        await brw.close();
    });
});
