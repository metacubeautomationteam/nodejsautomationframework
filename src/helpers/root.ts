import { launch, BrowserContext, Browser, Page } from "puppeteer";
import * as dotenv from 'dotenv';
import { getHeadless, getIncognitoBrowser, getPuppeteerLaunchOptions } from "./env";
dotenv.config({ path: '.env' });

export class BrowserTab {

    async getBrowser(): Promise<Browser> {
        const browser: Browser = await launch(getPuppeteerLaunchOptions(getHeadless()));    
        return browser;
    }

    async getPage(browser: Browser): Promise<Page> {
        const context: BrowserContext = getIncognitoBrowser() ? await browser.createIncognitoBrowserContext() : await browser.browserContexts()[0];
        const page: Page = await context.newPage();
        return page;
    }
}
