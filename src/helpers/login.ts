import { Page, ElementHandle } from "puppeteer";
import { getUserName, getPassword, getUrl } from "./env";

interface SelectorValue {
    selector: string;
    value: string;
}

export class Login{

    usernameSelector: SelectorValue = { selector: 'input#username', value: getUserName()!};
    passwordSelector: SelectorValue = { selector: 'input#password', value: getPassword()!};
    loginSelector: string = 'input#Login';

    async login(page: Page){
        await page.goto(getUrl()!);
        await page.waitForSelector(this.usernameSelector.selector);
        await page.type(this.usernameSelector.selector, this.usernameSelector.value);
        await page.type(this.passwordSelector.selector, this.passwordSelector.value);
        await page.click(this.loginSelector);
        await page.waitForSelector('nav[class*="appLauncher slds-context-bar__icon-action"]');
    }  
}