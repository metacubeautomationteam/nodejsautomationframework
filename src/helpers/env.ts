import * as dotenv from 'dotenv';
import { LaunchOptions } from 'puppeteer';
dotenv.config({ path: '.env' });

export function getHeadless(){
   return  process.env.HEADLESS === 'true';
}

export function getIncognitoBrowser(){
    return process.env.INCOGNITO_BROWSER === 'true';
}

export function getUserName(){
    return process.env.SF_USERNAME;
}

export function getPassword(){
    return process.env.SF_PASSWORD;
}

export function getUrl(){
    return process.env.SF_URL;
}

export function getPuppeteerLaunchOptions(headless?: boolean): LaunchOptions {
	return {
        defaultViewport: {
            width: 1920,
            height : 1080
        },
        
        args: [
			'--window-size=1920,1080',
		],

		headless,
		slowMo: 100,
		timeout: 0
	};
}